# Infos étudiant :

Par binôme Nom Prénom:
 * Lafarge Marc-Antoine
 * Vriet Paul
 

[![pipeline status](http://gitlab.isima.fr/malafarge/le-projet/badges/master/pipeline.svg)](http://gitlab.isima.fr/malafarge/le-projet/commits/master)
[![coverage report](http://gitlab.isima.fr/malafarge/le-projet/badges/master/coverage.svg)](http://gitlab.isima.fr/malafarge/le-projet/commits/master)

# TP 3

Le but de ce TP est de mettre en place un processus d'intégration continue en se basant sur le projet précédent.

## 1 Création d'un espace projet dédié sous gitlab
1. Pour mettre en place une chaine d'intégration continue initialisez un nouveau projet sous [GitLab](https://gitlab.isima.fr).
1. Ajouter dans la liste des développeurs votre binôme. (Settings/Members)
1. Ajouter en reporter `tjouve` et `iia_samorel`.
1. Ajoutez ce compte rendu comme readme de votre projet.
    * Vous pouvez le faire directement depuis l'interface avec *new File*
    * Copier le code markdown, il est possible d'editer le document via l'interface web directement sur gitlab
    * **Renseignez vos noms en haut du compte rendu**
1. Ajoutez une licence pour indiquer sous quels termes votre production est disponible
    * Cliquez sur **Add License**, il est possible de rédiger soi-même le contenu, ou de selectionner une licence existante (Apache, MIT, GNU ...). Le site [choose a license](https://choosealicense.com/licenses/) propose un comparatif entre les différentes liences les plus courantes.


## 2 Initialisation du projet
Pour prendre en charge la gestion des dépendances et l'automatisation de la construction de l'application nous utiliserons `Maven`.
Pour démarrer rapidement le projet nous allons faire du *scaffolding*. Pour ce faire nous utiliserons [Spring Boot](https://projects.spring.io/spring-boot/). Et plus particuliérement [Spring initializr](https://start.spring.io/).

### 2.1 Spring Initializer
1. Créer un squelette d'application avec [Spring initializr](https://start.spring.io/) :
    1. choisissez comme groupe id **isima.F2**
    2. comme nom d'artefact vous saisirez **TP3.`nom binome`**
    3. téléchargez ce template de projet.
1. clonez votre nouveau projet localement
    1. dézippez l'archive dans votre repo local, ce dernier ne devrait contenir que l'arborescence des sources, le pom et votre readme
    1. commitez et pushez sur le serveur


### 2.2 Utilisation de maven
Avant de reporter le code du TP2 nous allons nous familiariser avec *Maven*

#### 2.2.1 Convention over configuration
Pour permettre de développer plus rapidement des applications une bonne pratique est de respecter certaines normes.

Dans notre cas en utilisant **Spring Initializer** nous avons rapidement généré un squelette d'application utilisant maven et fournissant un certain nombre de fonctionnalitées transverses.
Notamment un framework de test **JUnit** et un framework de logging **Logback**


* A quoi correspondent les fichiers présents dans le zip ?
    * pom.xml
    * .gitignore
    * décrire l'arborescence de répertoire

Le fichier **pom.xml** est un fichier de configuaration propre à chaque projet utilisé par Maven. Il contient notament le nom du projet, sa vertion, les dépendances et d'autre éléments. Il est ecrit en xml d'ou son extention. 

Le **.gitignore** quand à lui permet de signialer à git les fichier à ne pas suivre et donc à ne pas commit, ni d'envoyer sur le repo distant.

*L'arborecence de projet ce présente comme ceci : 
    * 
    *
    *
    

#### 2.2.2 Gestion des dépendances

Dans le nouveau projet on retrouve une classe *Application.java* sous *src/main/java* dans le package *isima.F2.TP3.`nom binome`*
* Dans cette classe créez un logger et loggez un `Hello Word` en *warning*

```java
private static final Logger logger = LoggerFactory.getLogger(DemoApplication.class);

public static void main(String[] args) {
    SpringApplication.run(DemoApplication.class, args);
    logger.warn("Hello Word");

}
```

* Exécutez votre programme

`Nous obtenons ceci comme execution :`

```
 .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v2.2.4.RELEASE)

2020-02-17 14:04:46.185  INFO 5616 --- [           main] isima.F2.TP3.lafarge_vriet.Application   : Starting Application on nanocryk-acer with PID 5616 (C:\Users\nanocryk\Desktop\Depot\Forge\TP3\le-projet\TP3.lafarge_vriet\target\classes started by nanocryk in C:\Users\nanocryk\Desktop\Depot\Forge\TP3\le-projet\TP3.lafarge_vriet)
2020-02-17 14:04:46.185  INFO 5616 --- [           main] isima.F2.TP3.lafarge_vriet.Application   : No active profile set, falling back to default profiles: default
2020-02-17 14:04:46.607  INFO 5616 --- [           main] isima.F2.TP3.lafarge_vriet.Application   : Started Application in 0.708 seconds (JVM running for 1.039)
2020-02-17 14:04:46.609  WARN 5616 --- [           main] isima.F2.TP3.lafarge_vriet.Application   : Hello Word
```

Vous remarquerez qu'il n'est pas nécessaire d'ajouter la dépendance vers un logger (ici *logback*). C'est une dépendance classique d'un projet java. Elle est déjà présente dans le pom de *spring boot*.

Pour illustrer la gestion des dépendances automatiques vous ajouterez une dépendance vers la librairie **commons-lang3** de **apache**
* Ajout de la dépendance *Apache commons lang3*
    * Pour ce faire recherche `commons-lang3` sur [MVNrepository](https://mvnrepository.com).
    * selectionnez la derniére version
    * copiez la déclaration et collez là dans votre *pom.xml*
    * Décrivrez rapidement le fonctionnement de la résolution de dépendance

La résolution de dépendance se sert de ce que nous avons rentré dans le pom.xml pour télécharger les fichier manquants.
Grâce au information <ArtifactID> et du <groupId> Maven va trouver ou télécharger le code manquant quand ce sera fait il va ensuite passer au téléchargement des dépendances de cet artifact.
Il peut télécharger autant de dépendances imbriqué il y a juste un probleme quand les dépendances sont cyclique. 
On peut spécifier aussi comment on gére le téléchargement de dépendances externes.
Nous pouvons préciser quand on aura besoin de cette dépendance aux test, à la compilation ou dans une autre étape.


* Utilisez la classe utilitaire `StringUtils` et sa méthode `reverse` pour afficher dans la log au niveau *debug* vos noms à la suite du Hello Word.
    * La ligne de log n'apparait pas dans la console car le niveau de trace par defaut est `WARN`.
    * [Spring boot logging](https://docs.spring.io/spring-boot/docs/current/reference/html/howto-logging.html) indique comment changer le niveau de trace. (Il n'est pas nécessaire d'ajouter la dépendance elle est déjà présente).
    * Un fois le niveau de trace par defaut changé vous devriez voir le résultat de votre commande.
    * Quel fichier de configuration est modifié ?

```
logger.debug(StringUtils.reverse("Vriet Paul, Lafarge Marc-Antoine"));
```
Nous ajoutons le ligne : 
```
logging.level.root=DEBUG
```
Dans le fichier qui se trouve dans : 

```
le-projet\TP3.lafarge_vriet\target\classes\application.properties
```

#### 2.2.3 Gestion du cycle de vie

Dans cette partie nous allons utiliser les commandes de build de manven. Ces commandes permettentent d'automatiser certaines taches.
[Maven Lyfe Cycle](https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html) décrit l'ensemble des instructions et l'enchainement des différentes phases de **Maven**.

L'archive générée via spring initializer contient un répertoire `.mvn` et 2 scripts `mvnw` et `mvnw.bat`. Le projet contient par defaut un *wrapper* qui permet d'avoir une installation locale de maven pour ne pas être dépendant du systéme sur lequel on développe.

Il est possible de lancer directement des taches maven en appellant le script (`mvnw` pour *linux* et `mvnw.bat` pour *windows* ).

Par example `./mvnw.bat clean` permet de supprimer les classes compilées dans le repertoire target.

En utilisant la ligne de commande nous allons compiler, tester et packager l'application sous la forme d'un jar.

##### 2.2.3.1 Compilation
* Quelle est la commande à lancer pour compiler l'application ?
* Quelle est la commande à lancer pour compiler & éxécuter les tests ?
* Quel sont les fichiers / répertoires générés par cette commande ?

Nous pouvons compiler en lançant : 
```
.mvnw compile 
```

Nous pouvons tester et compiler en lançant (car il esécute toujours la compilation avant les test) : 
```
.mvnw test 
```
Le dossier généré est :
```
TP3.lafarge_vriet\target\test-classes
```

Le fichier généré est : 
```
TP3.lafarge_vriet\target\target\test-classes\isima\F2\TP3\lafarge_vriet\ApplicationTests.class

```

##### 2.2.3.2 Packaging
* Quelle est la commande à lancer ?
* Quel sont les fichiers / répertoires générés par cette commande ?

Nous pouvons packager en lançant : 
```
./mvnw package
```

Les fichiers générés sont les suivants :

```
TP3.lafarge_vriet\target\TP3.lafarge_vriet-0.01-SNAPSHOT.jar
```

### 2.3 Reportez le code du TP2           
* Reportez vos classes d'implémentation et de test dans le nouveau projet
    * Votre implémentation dans le repertoire src/main/java/*pakcage_name*/*Application*
    * Votre classe de test dans le repertoire src/test/java/*pakcage_name*/*ApplicationTests*
* Effectuez le packaging de votre application pour lancer la compilation et les tests.

```
[INFO] Scanning for projects...
[INFO]
[INFO] ---------------------< isima.F2:TP3.lafarge_vriet >---------------------
[INFO] Building TP3.lafarge_vriet 0.0.1-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
[INFO]
[INFO] --- maven-resources-plugin:3.1.0:resources (default-resources) @ TP3.laf
rge_vriet ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 1 resource
[INFO] Copying 0 resource
[INFO]
[INFO] --- maven-compiler-plugin:3.8.1:compile (default-compile) @ TP3.lafarge_
riet ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 3 source files to C:\Users\nanocryk\Desktop\Depot\Forge\TP3\le
projet\TP3.lafarge_vriet\target\classes
[INFO]
[INFO] --- maven-resources-plugin:3.1.0:testResources (default-testResources) @
TP3.lafarge_vriet ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory C:\Users\nanocryk\Desktop\Depot\Forg
\TP3\le-projet\TP3.lafarge_vriet\src\test\resources
[INFO]
[INFO] --- maven-compiler-plugin:3.8.1:testCompile (default-testCompile) @ TP3.
afarge_vriet ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 2 source files to C:\Users\nanocryk\Desktop\Depot\Forge\TP3\le
projet\TP3.lafarge_vriet\target\test-classes
[INFO]
[INFO] --- maven-surefire-plugin:2.22.2:test (default-test) @ TP3.lafarge_vriet
---
[INFO]
[INFO] -------------------------------------------------------
[INFO]  T E S T S
[INFO] -------------------------------------------------------
[INFO] Running isima.F2.TP3.lafarge_vriet.ApplicationTests
15:15:50.970 [main] DEBUG org.springframework.test.context.BootstrapUtils - Ins
antiating CacheAwareContextLoaderDelegate from class [org.springframework.test.
ontext.cache.DefaultCacheAwareContextLoaderDelegate]
15:15:50.970 [main] DEBUG org.springframework.test.context.BootstrapUtils - Ins
antiating BootstrapContext using constructor [public org.springframework.test.c
ntext.support.DefaultBootstrapContext(java.lang.Class,org.springframework.test.
ontext.CacheAwareContextLoaderDelegate)]
15:15:51.017 [main] DEBUG org.springframework.test.context.BootstrapUtils - Ins
antiating TestContextBootstrapper for test class [isima.F2.TP3.lafarge_vriet.Ap
licationTests] from class [org.springframework.boot.test.context.SpringBootTest
ontextBootstrapper]
15:15:51.033 [main] INFO org.springframework.boot.test.context.SpringBootTestCo
textBootstrapper - Neither @ContextConfiguration nor @ContextHierarchy found fo
 test class [isima.F2.TP3.lafarge_vriet.ApplicationTests], using SpringBootCont
xtLoader
15:15:51.033 [main] DEBUG org.springframework.test.context.support.AbstractCont
xtLoader - Did not detect default resource location for test class [isima.F2.TP
.lafarge_vriet.ApplicationTests]: class path resource [isima/F2/TP3/lafarge_vri
t/ApplicationTests-context.xml] does not exist
15:15:51.033 [main] DEBUG org.springframework.test.context.support.AbstractCont
xtLoader - Did not detect default resource location for test class [isima.F2.TP
.lafarge_vriet.ApplicationTests]: class path resource [isima/F2/TP3/lafarge_vri
t/ApplicationTestsContext.groovy] does not exist
15:15:51.033 [main] INFO org.springframework.test.context.support.AbstractConte
tLoader - Could not detect default resource locations for test class [isima.F2.
P3.lafarge_vriet.ApplicationTests]: no resource found for suffixes {-context.xm
, Context.groovy}.
15:15:51.033 [main] INFO org.springframework.test.context.support.AnnotationCon
igContextLoaderUtils - Could not detect default configuration classes for test
lass [isima.F2.TP3.lafarge_vriet.ApplicationTests]: ApplicationTests does not d
clare any static, non-private, non-final, nested classes annotated with @Config
ration.
15:15:51.080 [main] DEBUG org.springframework.test.context.support.ActiveProfil
sUtils - Could not find an 'annotation declaring class' for annotation type [or
.springframework.test.context.ActiveProfiles] and class [isima.F2.TP3.lafarge_v
iet.ApplicationTests]
15:15:51.158 [main] DEBUG org.springframework.context.annotation.ClassPathScann
ngCandidateComponentProvider - Identified candidate component class: file [C:\U
ers\nanocryk\Desktop\Depot\Forge\TP3\le-projet\TP3.lafarge_vriet\target\classes
isima\F2\TP3\lafarge_vriet\Application.class]
15:15:51.158 [main] INFO org.springframework.boot.test.context.SpringBootTestCo
textBootstrapper - Found @SpringBootConfiguration isima.F2.TP3.lafarge_vriet.Ap
lication for test class isima.F2.TP3.lafarge_vriet.ApplicationTests
15:15:51.283 [main] DEBUG org.springframework.boot.test.context.SpringBootTestC
ntextBootstrapper - @TestExecutionListeners is not present for class [isima.F2.
P3.lafarge_vriet.ApplicationTests]: using defaults.
15:15:51.283 [main] INFO org.springframework.boot.test.context.SpringBootTestCo
textBootstrapper - Loaded default TestExecutionListener class names from locati
n [META-INF/spring.factories]: [org.springframework.boot.test.mock.mockito.Mock
toTestExecutionListener, org.springframework.boot.test.mock.mockito.ResetMocksT
stExecutionListener, org.springframework.boot.test.autoconfigure.restdocs.RestD
csTestExecutionListener, org.springframework.boot.test.autoconfigure.web.client
MockRestServiceServerResetTestExecutionListener, org.springframework.boot.test.
utoconfigure.web.servlet.MockMvcPrintOnlyOnFailureTestExecutionListener, org.sp
ingframework.boot.test.autoconfigure.web.servlet.WebDriverTestExecutionListener
 org.springframework.test.context.web.ServletTestExecutionListener, org.springf
amework.test.context.support.DirtiesContextBeforeModesTestExecutionListener, or
.springframework.test.context.support.DependencyInjectionTestExecutionListener,
org.springframework.test.context.support.DirtiesContextTestExecutionListener, o
g.springframework.test.context.transaction.TransactionalTestExecutionListener,
rg.springframework.test.context.jdbc.SqlScriptsTestExecutionListener, org.sprin
framework.test.context.event.EventPublishingTestExecutionListener]
15:15:51.298 [main] DEBUG org.springframework.boot.test.context.SpringBootTestC
ntextBootstrapper - Skipping candidate TestExecutionListener [org.springframewo
k.test.context.web.ServletTestExecutionListener] due to a missing dependency. S
ecify custom listener classes or make the default listener classes and their re
uired dependencies available. Offending class: [javax/servlet/ServletContext]
15:15:51.298 [main] DEBUG org.springframework.boot.test.context.SpringBootTestC
ntextBootstrapper - Skipping candidate TestExecutionListener [org.springframewo
k.test.context.transaction.TransactionalTestExecutionListener] due to a missing
dependency. Specify custom listener classes or make the default listener classe
 and their required dependencies available. Offending class: [org/springframewo
k/transaction/interceptor/TransactionAttributeSource]
15:15:51.298 [main] DEBUG org.springframework.boot.test.context.SpringBootTestC
ntextBootstrapper - Skipping candidate TestExecutionListener [org.springframewo
k.test.context.jdbc.SqlScriptsTestExecutionListener] due to a missing dependenc
. Specify custom listener classes or make the default listener classes and thei
 required dependencies available. Offending class: [org/springframework/transac
ion/interceptor/TransactionAttribute]
15:15:51.298 [main] INFO org.springframework.boot.test.context.SpringBootTestCo
textBootstrapper - Using TestExecutionListeners: [org.springframework.test.cont
xt.support.DirtiesContextBeforeModesTestExecutionListener@51c693d, org.springfr
mework.boot.test.mock.mockito.MockitoTestExecutionListener@6a57ae10, org.spring
ramework.boot.test.autoconfigure.SpringBootDependencyInjectionTestExecutionList
ner@766653e6, org.springframework.test.context.support.DirtiesContextTestExecut
onListener@4e07b95f, org.springframework.test.context.event.EventPublishingTest
xecutionListener@28b46423, org.springframework.boot.test.mock.mockito.ResetMock
TestExecutionListener@7fc4780b, org.springframework.boot.test.autoconfigure.res
docs.RestDocsTestExecutionListener@3b79fd76, org.springframework.boot.test.auto
onfigure.web.client.MockRestServiceServerResetTestExecutionListener@48c76607, o
g.springframework.boot.test.autoconfigure.web.servlet.MockMvcPrintOnlyOnFailure
estExecutionListener@43599640, org.springframework.boot.test.autoconfigure.web.
ervlet.WebDriverTestExecutionListener@1f81aa00]
15:15:51.298 [main] DEBUG org.springframework.test.context.support.AbstractDirt
esContextTestExecutionListener - Before test class: context [DefaultTestContext
3ddc6915 testClass = ApplicationTests, testInstance = [null], testMethod = [nul
], testException = [null], mergedContextConfiguration = [MergedContextConfigura
ion@704deff2 testClass = ApplicationTests, locations = '{}', classes = '{class
sima.F2.TP3.lafarge_vriet.Application}', contextInitializerClasses = '[]', acti
eProfiles = '{}', propertySourceLocations = '{}', propertySourceProperties = '{
rg.springframework.boot.test.context.SpringBootTestContextBootstrapper=true}',
ontextCustomizers = set[org.springframework.boot.test.context.filter.ExcludeFil
erContextCustomizer@3943a2be, org.springframework.boot.test.json.DuplicateJsonO
jectContextCustomizerFactory$DuplicateJsonObjectContextCustomizer@71c3b41, org.
pringframework.boot.test.mock.mockito.MockitoContextCustomizer@0, org.springfra
ework.boot.test.web.client.TestRestTemplateContextCustomizer@6c779568, org.spri
gframework.boot.test.autoconfigure.properties.PropertyMappingContextCustomizer@
, org.springframework.boot.test.autoconfigure.web.servlet.WebDriverContextCusto
izerFactory$Customizer@19d481b], contextLoader = 'org.springframework.boot.test
context.SpringBootContextLoader', parent = [null]], attributes = map[[empty]]],
class annotated with @DirtiesContext [false] with mode [null].
15:15:51.314 [main] DEBUG org.springframework.test.context.support.DependencyIn
ectionTestExecutionListener - Performing dependency injection for test context
[DefaultTestContext@3ddc6915 testClass = ApplicationTests, testInstance = isima
F2.TP3.lafarge_vriet.ApplicationTests@31920ade, testMethod = [null], testExcept
on = [null], mergedContextConfiguration = [MergedContextConfiguration@704deff2
estClass = ApplicationTests, locations = '{}', classes = '{class isima.F2.TP3.l
farge_vriet.Application}', contextInitializerClasses = '[]', activeProfiles = '
}', propertySourceLocations = '{}', propertySourceProperties = '{org.springfram
work.boot.test.context.SpringBootTestContextBootstrapper=true}', contextCustomi
ers = set[org.springframework.boot.test.context.filter.ExcludeFilterContextCust
mizer@3943a2be, org.springframework.boot.test.json.DuplicateJsonObjectContextCu
tomizerFactory$DuplicateJsonObjectContextCustomizer@71c3b41, org.springframewor
.boot.test.mock.mockito.MockitoContextCustomizer@0, org.springframework.boot.te
t.web.client.TestRestTemplateContextCustomizer@6c779568, org.springframework.bo
t.test.autoconfigure.properties.PropertyMappingContextCustomizer@0, org.springf
amework.boot.test.autoconfigure.web.servlet.WebDriverContextCustomizerFactory$C
stomizer@19d481b], contextLoader = 'org.springframework.boot.test.context.Sprin
BootContextLoader', parent = [null]], attributes = map[[empty]]]].
15:15:51.345 [main] DEBUG org.springframework.test.context.support.TestProperty
ourceUtils - Adding inlined properties to environment: {spring.jmx.enabled=fals
, org.springframework.boot.test.context.SpringBootTestContextBootstrapper=true,
server.port=-1}

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v2.2.4.RELEASE)

2020-02-17 15:15:51.705  INFO 7976 --- [           main] i.F2.TP3.lafarge_vriet
ApplicationTests  : Starting ApplicationTests on nanocryk-acer with PID 7976 (s
arted by nanocryk in C:\Users\nanocryk\Desktop\Depot\Forge\TP3\le-projet\TP3.la
arge_vriet)
2020-02-17 15:15:51.705  INFO 7976 --- [           main] i.F2.TP3.lafarge_vriet
ApplicationTests  : No active profile set, falling back to default profiles: de
ault
2020-02-17 15:15:52.189  INFO 7976 --- [           main] i.F2.TP3.lafarge_vriet
ApplicationTests  : Started ApplicationTests in 0.838 seconds (JVM running for
.9)
[INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.75 s -
in isima.F2.TP3.lafarge_vriet.ApplicationTests
[INFO] Running isima.F2.TP3.lafarge_vriet.CalculateurTest
[INFO] Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0 s - in
isima.F2.TP3.lafarge_vriet.CalculateurTest
[INFO]
[INFO] Results:
[INFO]
[INFO] Tests run: 3, Failures: 0, Errors: 0, Skipped: 0
[INFO]
[INFO]
[INFO] --- maven-jar-plugin:3.1.2:jar (default-jar) @ TP3.lafarge_vriet ---
[INFO] Building jar: C:\Users\nanocryk\Desktop\Depot\Forge\TP3\le-projet\TP3.la
arge_vriet\target\TP3.lafarge_vriet-0.0.1-SNAPSHOT.jar
[INFO]
[INFO] --- spring-boot-maven-plugin:2.2.4.RELEASE:repackage (repackage) @ TP3.l
farge_vriet ---
[INFO] Replacing main artifact with repackaged archive
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  8.172 s
[INFO] Finished at: 2020-02-17T15:15:54+01:00
[INFO] ------------------------------------------------------------------------
``` 

## 3 Intégration continue

Gitlab permet de faire de l'intégration continue sur votre code.
Nous allons mettre en place un job permettant de réaliser la compilation,les test et le packaging à chaque commit en utilisant [Gitlab CI](https://about.gitlab.com/features/gitlab-ci-cd/)

### 3.1 Mettre en place une intégration continue en utilisant gitlab-ci

Liens utiles:
* [Gitlab CI](https://docs.gitlab.com/ce/ci/yaml/README.html#gitlab-ci-yml)

Sur la page d'acceuil de votre projet, à coté du bouton pour ajouter une licence, il y a un acces rapide pour créer à partir d'un template un fichier `.gitlab-ci.yml`.
On utilisera un template maven pour initialiser le descriptif des actions à réaliser.

* Désormais dans la partie CI/CD de votre projet vous pouvez voir vos pipelines en cours d'éxécution ou passé et leurs status.
* Le yml par defaut devrait faire echouer votre pipeline.
* Quelles est l'erreur ?

Premiere erreur je n'avais le projet dans le dossier directement 
```
The goal you specified requires a project to execute but there is no POM in this directory
```

```
Failed to execute goal org.apache.maven.plugins:maven-compiler-plugin:3.8.1:compile (default-compile) on project TP3.lafarge_vriet: Fatal error compiling: invalid flag: -parameters -> [Help 1]
```

* Le script template proposé par defaut est relativement complexe.
* Pour la suite nous utiliserons le template suivant :

```yaml
variables:
  # This will supress any download for dependencies and plugins or upload messages which would clutter the console log.
  # `showDateTime` will show the passed time in milliseconds. You need to specify `--batch-mode` to make this work.
  MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
  # As of Maven 3.3.0 instead of this you may define these options in `.mvn/maven.config` so the same config is used
  # when running from the command line.
  # `installAtEnd` and `deployAtEnd` are only effective with recent version of the corresponding plugins.
  MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true"

# Cache downloaded dependencies and plugins between builds.
# To keep cache across branches add 'key: "$CI_JOB_REF_NAME"'
cache:
  paths:
    - .m2/repository

#Definition des différentes étapes présent dans le pipeline
stages:
  - build
  - test

#Définition du job de compilation
compilation:
  image: maven:3.3.9-jdk-8
  stage: build
  script:
    - echo "A Completer";exit 1


validation:
  image: maven:3.3.9-jdk-8
  stage: test
  script:
      - echo "A Completer";exit 1
```

* Remplacez le `.gitlab-ci.yml` par celui-ci et completez le avec les commandes maven précédement utilisées pour réaliser la phase de build et de test

* Completez le `.gitlab-ci.yml` pour inclure un nouveau stage de packaging qui vas construire le jar de votre application

```
packaging:
  image: maven:3.3.9-jdk-8
  script:
    - mvn package
```

`Quels sont les lignes que vous avez rajouté dans votre déclaration de pipeline ?`   

Pour le build
```
-mvn compile 
```


Pour les test
```
-mvn test 
```

#### 3.1.1 Badge

**Gilab** permet d'afficher dans votre readme le status de votre pipeline dynamiquement pour celà :
* Rendez vous dans la partie [Settings > CI/CD > General pipelines settings](https://docs.gitlab.com/ee/user/project/pipelines/settings.html) de votre projet. A l'aide de la documentation sur les [badges](https://docs.gitlab.com/ee/user/project/pipelines/settings.html#badges)
rajoutez un bdage indiquant le status de votre pipeline de build dans votre readme.

### 3.2 Couverture de code

Nous allons ajouter un outil permettant de réaliser des statistiques sur la qualité des tests. Il sagit ici de [jacoco](http://www.eclemma.org/jacoco/) une librairie permettant de réaliser des statistiques sur le taux de couverture de code par les tests unitaires.

* Ce plugin lors de la compilation vas modifier votre code pour injecter du code permettant de savoir quelles lignes sont exécutées ou non.
    * Pour ce faire il faut rajouter dans votre **pom.xml**, dans la partie *build/pulgins*, la déclaration suivante :

```xml
<plugin>
    <groupId>org.jacoco</groupId>
    <artifactId>jacoco-maven-plugin</artifactId>
    <version>0.8.0</version>
    <executions>
        <execution>
            <id>default-prepare-agent</id>
            <goals>
                <goal>prepare-agent</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

* Nous allons rajouter au readme du projet un badge indiquant le % de code couvert.
    * Sur gitlab, dans les settings de la partie CI/CD dans General pipeline settings, vous trouverez une zone de saisie pour **Test coverage parsing**.
En effet il est nécessaire d'indiquer à **Gitlab CI** où trouver l'information sur le pourcentage de code couvert : `A regular expression that will be used to find the test coverage output in the job trace.`
    * Cette information se trouve dans le rapport généré par jacoco. Par defaut le rapport n'est pas généré. Il faut donc modifier le paramètrage du plugin pour lui demander de générer le rapport.
Dans le code suivant on indique au plugin qu'il doit exécuter sa tache **report** lorsque de la phase **test** de maven.   

```xml
<execution>
    <id>default-report</id>
    <phase>test</phase>
    <goals>
        <goal>report</goal>
    </goals>
</execution>
```

* Le rapport est généré dans le repertoire target/site/jacoco sous la forme d'un fichier html.
    * Il faut dans la partie settings de votre CI indiquer l'expression réguliére permettant de retrouver le % de code couvert. Utilisez `Total.*?([0-9]{1,3})%`.
    * Ajoutez à votre `.gitlab-ci.yml` la commande permettant de faire apparaitre le contenu de ce fichier dans la log de votre job.

`Comment affichez vous l'information dans la log du job ?`

* Rajoutez en début de votre readme le badge indiquant le pourcentage de couverture de code.


### 3.3 Analyse de code avec Sonar
Nous allons mettre en place à chaque éxécution de notre pipeline une analyse sonar.

Le serveur sonar à utiliser se trouve à l'adresse suivante : [ISIMA Sonar](http://40.89.147.107/)

Le compte permettant d'acceder à l'application est `isima`, le mot de passe `isima2019`.

* à la suite de la phase de test ajoutez dans votre job une analyse Sonar.
* Pour exécuter une analyse sonar il suffit de lancer la commande :

```
mvn sonar:sonar -Dsonar.host.url=http://40.89.147.107 -Dsonar.login=TOKEN
```

* Le token pour le user `isima` est `99ae2bca252a19b71d0886ed8338c768d5c2da95`.
* Une fois la première analyse lancée, rendez vous sur l'application pour constater quels sont les anomalies détectées et les indicateurs de qualimétrie de votre code.

> Après la 1ere analyse, o trouve 1bug, 1 security hotspot, 13 code smells et 0% de coverage, car j'ai omis les tests dans l'execution de sonar. On va tenter de les ajouter avant de régler le bug.

> Vendredi (jour prévu de rendu du rapport) Je ne parviens pas à passer le dossier de test à sonar. D'où la couverture de 0%. Je retenterais dans le week-end.

* Corrigez votre code en conséquence pour obtenir la meilleur note possible.
* Il est possible de spécifier directement dans le pom.xml l'adresse du serveur cible
    * pour ce faire on ajoutera dans la balise properties une entrée tel que :

    ```xml
    <sonar.host.url>http://40.89.147.107</sonar.host.url>
    ```
    * on peux alors appeler directement `mvn sonar:sonar -Dsonar.login=TOKEN` l'URL du serveur est prise dans les properties de votre projet maven

* Pour le login il est possible de faire de même, mais en terme de sécurité ce n'est pas une bonne idée d'indiquer le token d'acces dans vos sources
    * Gitlab-ci permet d'injecter de manière caché certaines propriétés [secret variables](https://gitlab.isima.fr/help/ci/variables/README#secret-variables)
    * Définissez une variable secrete contenant le login et utilisez la dans votre .gitlab-ci.yml 
