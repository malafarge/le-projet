package isima.F2.TP3.lafarge_vriet;
public class Calculateur {

	public Calculateur() {

	 }

	 static public Double indemnites(Double distance) {

		 if (distance == null) {
			 throw new NullPointerException();
		 }

		 Double indem = 0.;

		 if (distance <= 0) {
			 return 0.;
		 }

		 if (distance < 10) {
			 return distance*1.50;
		 }

		 if (distance <= 39) {
			 return distance*0.40;
		 }

		 if (distance <= 60) {
			 return distance*0.55;
		 }

		 indem = 33.;
		 distance -= 60;

		 int i = 0;
		 while (distance > 20) {
			 distance -= 20;
			 i++;
		 }

		 indem += i * 6.81;

		 return indem;
	 }
}