package isima.F2.TP3.lafarge_vriet;


import org.slf4j.LoggerFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import ch.qos.logback.classic.Logger;
import isima.F2.TP3.lafarge_vriet.Calculateur;


public class CalculateurTest {

	 @Test
	 public void indemnitesTests() {
		 Logger logger = (Logger) LoggerFactory.getLogger("Fonction test Junit indemnitesTests");
		 logger.debug("Démarage des tests de la fonction.");

		 Double data[] = new Double[] {-1., 0., 0.5, 5., 10., 15., 39., 50., 60., 70., 80., 90., 100., 110., 120.};

		 Double results[] = new Double[] {0., 0., 0.75, 7.5, 4., 6., 15.6, 27.5, 33., 33., 33., 39.81, 39.81, 46.62, 46.62};

		 for (int i=0; i<data.length; ++i) {
			 logger.debug("Donnée :" + data[i].toString() + "\t indemnité :" + Calculateur.indemnites(data[i]));
			 Double expected = results[i];
			 Double actual = Calculateur.indemnites(data[i]);
			 Assertions.assertEquals(expected, actual, 0.01);
		 }

		 Assertions.assertThrows(NullPointerException.class, () -> { Calculateur.indemnites(null); });

		 logger.debug("Fin des tests.");

	}

	 @Test
	 public void indemnitesNullTests() {
		 Logger logger = (Logger) LoggerFactory.getLogger("Fonction test Junit indemnitesNullTests");
		 logger.debug("Démarage des tests de la fonction.");

		 Assertions.assertThrows(NullPointerException.class, () -> { Calculateur.indemnites(null); });

		 logger.debug("Fin des tests.");

	}

}

